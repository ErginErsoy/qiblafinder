package com.erginersoy.compass;

import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import static android.view.Gravity.CENTER;

public class MainActivity extends AppCompatActivity implements SensorEventListener {


    private ImageView compassview1;
    private ImageView compassview2;

    private float[] mGravity = new float[3];
    private float[] mGeomagnetic = new float[3];
    private float azimuth = 0f;
    private float currentAzimuth = 0f;
    private float lastRotation = 0f;
    private SensorManager sensorManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        compassview1 = findViewById(R.id.compassview1);
        compassview2 = findViewById(R.id.compassview2);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);


    }

    private void calculateBear() {
        Location istanbul = new Location("");
        istanbul.setLatitude(41.015137);
        istanbul.setLongitude(28.979530);

        Location macca = new Location("");
        macca.setLatitude(21.422487);
        macca.setLongitude(39.826206);

        float compass2azimuth = azimuth;

        GeomagneticField geomagneticField = new GeomagneticField(
                (float) istanbul.getLatitude(),
                (float) istanbul.getLongitude(),
                (float) istanbul.getAltitude(), System.currentTimeMillis());

        compass2azimuth -= geomagneticField.getDeclination();

        float bearTo = istanbul.bearingTo(macca);
        if (bearTo < 0) bearTo = bearTo + 360;

        float rotation = bearTo - compass2azimuth;
        if (rotation < 0) rotation = rotation + 360;

        rotation = rotation % 360;
        float animToDegree = getShortestPathEndPoint(lastRotation, rotation);

        final RotateAnimation rotateAnimation = new RotateAnimation(lastRotation, animToDegree,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(500);
        rotateAnimation.setFillAfter(true);

        lastRotation = rotation;
        compassview2.startAnimation(rotateAnimation);


    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_GAME);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        final float alpha = 0.97f;
        synchronized (this) {
            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                mGravity[0] = alpha * mGravity[0] + (1 - alpha) * event.values[0];
                mGravity[1] = alpha * mGravity[1] + (1 - alpha) * event.values[1];
                mGravity[2] = alpha * mGravity[2] + (1 - alpha) * event.values[2];
            }
            if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
                mGeomagnetic[0] = alpha * mGeomagnetic[0] + (1 - alpha) * event.values[0];
                mGeomagnetic[1] = alpha * mGeomagnetic[1] + (1 - alpha) * event.values[1];
                mGeomagnetic[2] = alpha * mGeomagnetic[2] + (1 - alpha) * event.values[2];
            }
            float[] R = new float[9];
            float[] I = new float[9];
            boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
            if (success) {
                float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);
                azimuth = (float) Math.toDegrees(orientation[0]);
                azimuth = (azimuth + 360) % 360;


                Animation animation = new RotateAnimation(-currentAzimuth, -azimuth, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                currentAzimuth = azimuth;
                animation.setDuration(500);
                animation.setRepeatCount(0);
                animation.setFillAfter(true);
                compassview1.startAnimation(animation);

                calculateBear();
            }
        }
    }

    private float getShortestPathEndPoint(float start, float end) {
        float delta = deltaRotation(start, end);
        float invertedDelta = invertedDelta(start, end);
        if (Math.abs(invertedDelta) < Math.abs(delta)) end = start + invertedDelta;
        return end;
    }

    private float deltaRotation(float start, float end) {
        return end - start;
    }

    private float invertedDelta(float start, float end) {
        float delta = end - start;
        if (delta < 0) end += 360;
        else end += -360;
        return end - start;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
